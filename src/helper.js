const insertString = (strOrigin, idx, rem, str) => strOrigin.slice(0, idx) + str + strOrigin.slice(idx + Math.abs(rem));

const handleTags = (strParam, mountTarget) => {
  let str = strParam.replace(/<script\b[^<]*(?:(?!<\/script>)<[^<]*)*<\/script>/gi, '');
  const regex = /<link(.*?)>/g;
  const strReplace = str.match(regex);

  if (strReplace) {
    strReplace.forEach((strItem) => {
      str = str.replace(strItem, '');
    });

    const indexHead = /<head>/.exec(str).index;
    const result = insertString(str, indexHead + 7, 0, strReplace.join('\n'));
    return result;
  }
  return `
    <!DOCTYPE html>
    <html><head>
    <link href="//cdn.shopify.com/s/files/1/0248/2330/0176/t/5/assets/theme.scss.css?v=12826658811228778362" rel="stylesheet" type="text/css" media="all" />
    <link href="https://cdn.xopify.com/page-builder/assets/sc-builder.css" rel="stylesheet" type="text/css" media="all" />
    </head>
      <body>
      <div id="${mountTarget.slice(1)}"></div>
      </body>
      </html>
    `;
};

export default handleTags;
